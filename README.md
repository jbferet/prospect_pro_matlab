# PROSPECT leaf model: Matlab version

# 1 Description
This is the matlab version of the leaf model PROSPECT. 
These codes actually allow to run PROSPECT-PRO, the latest version published in 2021, or PROSPECT-D, the previous version.

# 2 Warning

## Difference between PROSPECT-PRO and PROSPECT-D
The main difference between the two versions is that:
- PROSPECT-D includes dry matter constituents as a unique input parameter, Cm. Cm is refered to as 'dry matter content' in PROSPECT papers, and better known as Leaf Mas per Area (LMA) by the plant community. 
- PROSPECT-PRO allows the separation of LMA into two main constituents: proteins, and carbon based constituents (CBC, which includes cellulose, lignin, starch, and all non structural and non structural carboydrates with have mnimal contribution in leaf nitrogen content)

In clear:

 <font size="4"> `LMA = CBC + Proteins` </font>


Therefore, 
- if users want to run PROSPECT-D, CBC and Prot should be set to 0.
- if users want to run PROSPECT-PRO, Cm should be set to 0.

## What about PROSPECT-4 and PROSPECT-5?
We strongly recommend using PROSPECT-D instead of PROSPECT-4 and PROSPECT-5, as the refractive index is relatively different between these versions.
The leaf optical properties simulated with the previous versions (4 and 5) showed artifacts due to the refractive index. 
Therefore: 
- we recommend setting anthocyanin content (Cant) to 0 to run an equivalent of PROSPECT-5.
- we recommend setting anthocyanin content (Cant) and carotenoid content (Cxc) to 0 to run an equivalent of PROSPECT-4.

# 3 Alternative distributions: 

We strongly recommend PROSPECT users to start using the R package [`prospect`](https://jbferet.gitlab.io/prospect/).
This R version includes both PROSPECT-D and PROSPECT-PRO version, as well as a set of functions to produce look up tables, run PROPECT inversion with optimized routines...

# 4 Citation

If you use **prospect**, please cite the following references for PROSPECT-PRO and PROSPECT-D:

Féret, J.-B., Berger, K., de Boissieu, F. & Malenovský, Z. (2021). PROSPECT-PRO for estimating content of nitrogen-containing leaf proteins and other carbon-based constituents. Remote Sensing of Environment. 252, 112173.  https://doi.org/10.1016/j.rse.2020.112173

Féret, J.-B., Gitelson, A.A., Noble, S.D. & Jacquemoud, S. (2017). PROSPECT-D: Towards modeling leaf optical properties through a complete lifecycle. Remote Sensing of Environment. 193, 204–215. http://dx.doi.org/10.1016/j.rse.2017.03.004

The inversion of PROSPECT using only directional-hemispherical reflectance or directional-hemispherical transmittance, and prior estimation of N (available in R package) is explained here: 

Spafford, L., le Maire, G., MacDougall, A., de Boissieu, F. & Féret, J.-B. (2021). Spectral subdomains and prior estimation of leaf structure improves PROSPECT inversion on reflectance or transmittance alone. Remote Sensing of Environment. 252, 112176.  https://doi.org/10.1016/j.rse.2020.112176
